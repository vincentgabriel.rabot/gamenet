﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviourPunCallbacks
{
    public float startHealth = 100;
    public float health;
    public Image healthbar;
    public bool isDead;
    
    void Start()
    {
        //health = 10;
        SetHealth();
    }

    [PunRPC]
    void SetHealth()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void TakeDamage(float damage)
    {
        if (health > 0)
        {
            health -= damage;
        }

        if (health <= 0)
        {
            photonView.RPC("Die", RpcTarget.AllBuffered);
        }

        healthbar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void Die()
    {
        if (isDead == false)
        {
            isDead = true;
            GetComponent<GameController>().PlayerDeath();
        }
    }
    
}
