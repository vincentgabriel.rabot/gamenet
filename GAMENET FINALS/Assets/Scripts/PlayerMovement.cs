﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;

    private float horizontalMove = 0;
    
    public float speed;

    private Rigidbody2D rb;

    private bool jump = false;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
       horizontalMove =  Input.GetAxis("Horizontal") * speed;

       if (Input.GetButtonDown("Jump"))
       {
           jump = true;
       }
    }

    
    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }
}
