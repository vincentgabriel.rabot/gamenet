﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class CountdownManager : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI timerText;

    public float timeToStartGame = 5f;
    
    // Start is called before the first frame update
    void Start()
    {
        timerText = GameManager.instance.timeText;
    }

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (timeToStartGame > 0)
            {
                timeToStartGame -= Time.deltaTime;
                photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartGame);
            }
            else if (timeToStartGame < 0)
            {
                photonView.RPC("StartRace", RpcTarget.AllBuffered);
            }
        }
    }
    
    [PunRPC]
    public void SetTime(float time)
    {
        if (time > 0)
        {
            timerText.text = time.ToString("F1");
        }
        else
        {
            timerText.text = "";
        }
    }
    
    [PunRPC]
    public void StartRace()
    {
        GetComponent<PlayerMovement>().enabled = photonView.IsMine;
        GetComponent<Shooting>().enabled = photonView.IsMine;

        this.enabled = false;
    }
}
