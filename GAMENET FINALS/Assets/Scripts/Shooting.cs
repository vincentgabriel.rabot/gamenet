﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Shooting : MonoBehaviourPunCallbacks
{
    public float fireRate;
    public bool canShoot = true;
    public Transform firePoint;
    public GameObject projectilePrefab;
    public SpriteRenderer sprite;
    
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

   
    void Update()
    {
        Fire();
    }

    public void Fire()
    {
        
        if (canShoot)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //PhotonNetwork.Instantiate("Projectile", firePoint.position, firePoint.rotation);
                Projectile projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation).GetComponent<Projectile>();
                projectile.direction = sprite.flipX ? Vector2.right : Vector2.left;
                canShoot = false;
                StartCoroutine(ShootDelay());
            }
        }
    }

    IEnumerator ShootDelay()
    {
        if (!canShoot)
        {
            yield return new WaitForSeconds(fireRate);
            canShoot = true;
        }
    }
}
