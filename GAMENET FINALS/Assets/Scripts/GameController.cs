﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class GameController : MonoBehaviourPunCallbacks
{
    public int deathOrder = 0;

    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte) RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;
            string nicknameOfFinishedPlayer = (string) data[0];
            deathOrder = (int) data[1];
            int viewId = (int) data[2];

            if (viewId == photonView.ViewID)
            {
                GameManager.instance.Win();
            }
            else if (viewId!= photonView.ViewID)
            {
                if (GameManager.instance.playerCount <= 1)
                {
                    GameManager.instance.Lose();
                }
            }
        }
    }

    public void PlayerDeath()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<PlayerMovement>().enabled = false;
        GetComponent<PlayerHealth>().enabled = false;
        GetComponent<Shooting>().enabled = false;

        GameManager.instance.playerCount--;
        deathOrder++;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] {nickName, deathOrder, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All, CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);
    }
}
