﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI name;
    
    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine)
        {
            name.text = photonView.Owner.NickName.ToString() + "(YOU)";
        }
        else
        {
            name.text = photonView.Owner.NickName.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
