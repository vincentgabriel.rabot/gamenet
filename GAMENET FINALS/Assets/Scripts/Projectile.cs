﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon.StructWrapping;
using Photon.Pun;
using UnityEngine;

public class Projectile : MonoBehaviourPunCallbacks
{
    public float projectileSpeed;

    public float damage;
    public Vector2 direction;
    public Rigidbody2D rb;
    
 
    void Start()
    {
        rb.velocity = transform.right * projectileSpeed;
        Destroy(gameObject, 5f);
    }

   
    void Update()
    {
        //this.transform.position += (Vector3)(direction * projectileSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player")) {
            other.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage
            );
            //other.GetComponent<PlayerHealth>().TakeDamage(damage);
            //Debug.Log("hit yourself");
            Debug.Log(damage); // put damage shit
        }
        Destroy(gameObject);
    }
}
