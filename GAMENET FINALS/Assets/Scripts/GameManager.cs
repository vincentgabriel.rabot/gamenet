﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks
{

    public GameObject playerPrefab;
    public Transform[] startingPos;
    public static GameManager instance = null;
    public TextMeshProUGUI timeText;
    public int playerCount;
    public GameObject[] winnerTextUi;
    public GameObject winUi;
    public GameObject loseUi;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad(gameObject);
    }
    
    void Start()
    {
        playerCount = 2;
        if (PhotonNetwork.IsConnectedAndReady)
        {
            playerCount = 2;

            //Debug.Log((int)playerSelectionNumber);
            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            Vector3 instantiatePosition = startingPos[actorNumber - 1].position;
            PhotonNetwork.Instantiate(playerPrefab.name, instantiatePosition, Quaternion.identity);
        }
    }

    public void OnExitButtonClicked()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LeaveLobby();
        PhotonNetwork.LoadLevel("LobbyScene");
    }

    public void Lose()
    {
        loseUi.SetActive(true);
        winUi.SetActive(false);
    }

    public void Win()
    {
        winUi.SetActive(true);
        loseUi.SetActive(false);
    }
    
}
