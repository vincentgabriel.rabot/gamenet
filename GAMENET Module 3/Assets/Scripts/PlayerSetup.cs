﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    public TextMeshProUGUI name;
    void Start()
    {
        name.text = photonView.Owner.NickName.ToString();
        if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<CountdownManager>().timerText = RacingGameManager.instance.timeText;
            this.camera = transform.Find("Camera").GetComponent<Camera>();
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = false;
            GetComponent<DeathRaceController>().enabled = false;
            camera.enabled = photonView.IsMine;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<CountdownManager>().timerText = DeathRaceManager.instance.timeText;
            this.camera = transform.Find("Camera").GetComponent<Camera>();
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = false;
            GetComponent<DeathRaceController>().enabled = photonView.IsMine;

        }
        
    }
    
}
