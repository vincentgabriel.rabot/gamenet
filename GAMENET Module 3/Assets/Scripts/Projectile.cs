﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Projectile : MonoBehaviourPunCallbacks
{
    public float projectileSpeed;

    public float damage;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector3.forward * projectileSpeed * Time.deltaTime);
    }
    
    
    void OnTriggerEnter(Collider col)
    {
        if (col.GetComponent<Shooting>() && col.GetComponent<PhotonView>().IsMine)
        {
            col.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
            Debug.Log(damage);
        }
        Destroy(gameObject);
    }
}
