﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Cache;
using System.Resources;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using TMPro;

public class Shooting : MonoBehaviourPunCallbacks
{
    private PunPlayerScores playerScore;
    public Camera camera;
    public TextMeshProUGUI victoryText;
    private GameObject lastHitObject;
    private bool isDead = false;
    private bool canShoot = true;
    public float fireRate;
    public float rayDamage;

    [Header("HP Related Stuff")]
    public float startHealth = 10;
    public float health;
    public Image healthBar;
    public Transform firePoint;


    // Start is called before the first frame update
    void Start()
    {
        photonView.RPC("SetHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    void SetHealth()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
    }

    // Update is called once per frame
    void Update()
    {
        Fire();
    }

    public void Fire()
    {
        
        if (canShoot)
        {
            if (Input.GetMouseButtonDown(0) && photonView.IsMine)
            {
                PhotonNetwork.Instantiate("Projectile", firePoint.position, firePoint.rotation);
            }

            canShoot = false;
            StartCoroutine(ShootDelay());
        }

        if (Input.GetMouseButtonDown(1) && photonView.IsMine)
        {
            RaycastHit hit;
            Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

            if (Physics.Raycast(ray, out hit, 200))
            {
                if (hit.collider.gameObject.CompareTag("Player") &&
                    !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, rayDamage);
                    if (hit.collider.gameObject.GetComponent<Shooting>().isDead)
                    {
                        //Victory shit
                        photonView.RPC("Victory", RpcTarget.AllBuffered);
                    }
                }
            }
        }

    }
    [PunRPC]
    public void TakeDamage(float damage)
    {
        if (health > 0)
        {
            health -= damage;
        }
        

        if (health <= 0)
        {
            photonView.RPC("Die", RpcTarget.All);
        }
        
        healthBar.fillAmount = health / startHealth;

        //Debug.Log(health);
    }
    
    
    [PunRPC]
    public void Die()
    {
        if (isDead == false)
        {
            isDead = true;
            GetComponent<DeathRaceController>().PlayerDeath();
        }
    }

    IEnumerator ShootDelay()
    {
        if (!canShoot)
        {
            yield return new WaitForSeconds(fireRate);
            canShoot = true;
        }
    }
    
}
