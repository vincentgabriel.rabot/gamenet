﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class DeathRaceController : MonoBehaviourPunCallbacks
{
    public int deathOrder = 0;
    
    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0 
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte) RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;
            string nicknameOfFinishedPlayer = (string) data[0];
            deathOrder = (int) data[1];
            int viewId = (int) data[2];
            

            Debug.Log(nicknameOfFinishedPlayer + " " + deathOrder);
            GameObject orderUiText = DeathRaceManager.instance.winnerTextUi[deathOrder - 1];
            orderUiText.SetActive(true);

            if (viewId == photonView.ViewID) // this is u
            {
                orderUiText.GetComponent<Text>().text = nicknameOfFinishedPlayer + " Died " + " (YOU)";
                orderUiText.GetComponent<Text>().color = Color.red;
                DeathRaceManager.instance.Lose();
                //this.gameObject.SetActive(false);
            }
            else
            {
                if (DeathRaceManager.instance.playerCount <= 1)
                {
                    DeathRaceManager.instance.Win();
                }

                orderUiText.GetComponent<Text>().text = nicknameOfFinishedPlayer + " Died ";
            }
        }
    }
        
    public void PlayerDeath()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false;
        GetComponent<Shooting>().enabled = false;
        
        DeathRaceManager.instance.playerCount--;
        deathOrder++;
        
        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] {nickName, deathOrder, viewId};
        
        

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOption);
    }
    
}
