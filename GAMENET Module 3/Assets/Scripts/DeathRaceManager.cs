﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathRaceManager : MonoBehaviour
{
    public GameObject[] vehiclePrefabs;
    public Transform[] startingPositions;
    public static DeathRaceManager instance = null;
    public Text timeText;
    public GameObject[] winnerTextUi;
    public GameObject winUi;
    public GameObject loseUi;
    public int playerCount;
    
    
    void Start()
    {
        winUi.SetActive(false);
        loseUi.SetActive(false);
        
        playerCount = PhotonNetwork.CountOfPlayers;
        if (PhotonNetwork.IsConnectedAndReady)
        {
            playerCount = PhotonNetwork.CountOfPlayers;
            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER,
                out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);
                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
        }
    }

    void Update()
    {
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad(gameObject);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        
    }

    public void Lose()
    {
        loseUi.SetActive(true);
    }

    public void Win()
    {
        winUi.SetActive(true);
    }

}
