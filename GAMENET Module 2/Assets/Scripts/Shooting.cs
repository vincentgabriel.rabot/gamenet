﻿using System.Collections;
using System.Collections.Generic;
using System.Resources;
using System.Security.Cryptography;
using ExitGames.Client.Photon.StructWrapping;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using TMPro;

public class Shooting : MonoBehaviourPunCallbacks
{
    private PunPlayerScores playerScore;
    public Camera camera;
    public GameObject hitEffectPrefab;
    public GameObject uiReference;
    public TextMeshProUGUI victoryText;
    private string lastHitText;
    private GameObject lastHitObject;
    public int kills = 0;
    private bool isDead = false;

    [Header("HP Related Stuff")] public float startHealth = 100;
    private float health;
    public Image healthBar;
    private Animator animator;
    
    
    // Start is called before the first frame update
    void Start()
    {
        victoryText = GameObject.Find("VictoryText").GetComponent<TextMeshProUGUI>();
        uiReference = GameObject.Find("Player Ui(Clone)");
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f,0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            //Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);
            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
                if(hit.collider.gameObject.GetComponent<Shooting>().isDead)
                {
                    kills++;
                    if (kills == 10)
                    {
                        //Victory shit
                        photonView.RPC("Victory", RpcTarget.AllBuffered);
                    }
                }
            }
        }

    }
    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        if (!isDead)
        {
            this.health -= damage;
            this.healthBar.fillAmount = health / startHealth;

            if (health <= 0)
            {
                Die(info);
                //Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
                //lastHitText = info.Sender.NickName.ToString();
                uiReference.transform.Find("KillFeedText").GetComponent<Text>().text =
                    lastHitText + " killed " + photonView.Owner.NickName.ToString();
                StartCoroutine(KillFeedDuration());
            }
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }
    
    [PunRPC]
    public void Die(PhotonMessageInfo info)
    {
        isDead = true;
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
        lastHitText = info.Sender.NickName.ToString();
        //Debug.Log("Kills: " + kills.ToString());
    }

    IEnumerator KillFeedDuration()
    {
        yield return new WaitForSeconds(5.0f);
        uiReference.transform.Find("KillFeedText").GetComponent<Text>().text = " ";
    }
    
    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText");
        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }
        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        this.transform.position = GameManager.Instance.spawnPoints[Random.Range(0, 9)].transform.position;
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        isDead = false;
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void Victory()
    {
        victoryText.text = photonView.Owner.NickName.ToString() + " HAS WON!!";
    }
}
